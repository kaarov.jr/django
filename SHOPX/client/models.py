from django.core.exceptions import ValidationError
from django.db import models


class Client(models.Model):
    name = models.CharField(max_length=50, verbose_name='Имя')
    surname = models.CharField(max_length=50, verbose_name='Фамилия')
    age = models.PositiveIntegerField(verbose_name='Возраст', default=10)
    address = models.CharField(verbose_name='Адрес', max_length=100)
    phone_number = models.CharField(verbose_name='Телефон номер', max_length=15)

    def __str__(self):
        return '{name} {surname}'.format(name=self.name, surname=self.surname)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
        unique_together = ['name', 'surname']

    def clean(self):
        p = self.phone_number

        check = ['700', '701', '705', '707', '709',
                 '772', '777', '775', '778', '779',
                 '500', '503', '505', '509', '550',
                 '555', '999', '995', '992']

        if len(p) == 12 or len(p) == 13:
            if p[:3] != '996':
                raise ValidationError('Не правильный ввод номер телефона: Введите 996!')
            elif p[3:6] not in check:
                raise ValidationError('Не правильный ввод номер телефона: Такого номера не существует!')
        else:
            raise ValidationError('Не правильный ввод номер телефона: Введите 13 или 12 значный символ!')