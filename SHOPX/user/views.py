from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect


def register(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        password2 = request.POST['password2']
        if password2 != password:
            messages.error(request, 'Passwords are not correct!')
            return redirect('register')
        user = User()
        user.username = username
        user.set_password(password)
        user.save()
    return render(request, 'user/register.html')


def login_own(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, 'You are logined!')
            return redirect('home')
        else:
            messages.success(request, 'You are not logined!')
            return redirect('login_own')
    else:
        if request.user.is_authenticated:
            return redirect('home')
    return render(request, 'user/login.html')


def comment(request):
    return render(request, 'user/comment.html')


def logout_own(request):
    return redirect('logout_own')


def notification(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, 'You are logined!')
            return redirect('home')
        else:
            messages.success(request, 'You are not logined!')
            return redirect('login_own')
    return render(request, 'user/notification.html')

