from django.urls import path, include
from user.views import *

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('register/', register, name='register'),
    path('login_shopx/', login_own, name='login_own'),
    path('logout/', logout_own, name='logout_own'),
    path('notification/', notification, name='notification'),
]
