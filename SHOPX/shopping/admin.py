from django.contrib import admin
from shopping.models import *
from django.db.models import QuerySet


@admin.action(description='Изменить Актив')
def change_active(self, request, qs: QuerySet):
    qs.update(active=False)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'image']
    list_display_links = list_display
    list_per_page = 5
    search_fields = ['name', ]
    ordering = ['name', ]


class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'image']
    list_display_links = list_display
    list_per_page = 5
    search_fields = ['name', ]
    filter = 'active'
    actions = ['change_active', ]


class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'cur', 'price', 'article', 'active', 'description', 'date']
    list_display_links = ['name', 'cur']
    search_fields = ['name', 'article']
    list_filter = ['name', 'price']
    # list_filter = ['sub_category', 'category',
    #                'active']
    list_per_page = 10
    ordering = ['date', ]
    actions = ['change_active', ]


class OrderInline(admin.TabularInline):
    model = ItemOrder
    extra = 0


class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderInline]


class FeedBACK(admin.ModelAdmin):
    list_display = ['name', ]
    list_display_links = list_display
    list_per_page = 5
    search_fields = ['name', ]
    ordering = ['name', ]


class CommentAdmin(admin.ModelAdmin):
    list_display = ['name', 'product', 'user', 'created_at']
    list_display_links = ['name', 'user']
    list_per_page = 5
    search_fields = ['name', ]
    ordering = ['-id', ]


admin.site.register(City)
admin.site.register(Brand)
admin.site.register(Category, CategoryAdmin)
admin.site.register(SubCategory, SubCategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(FeedBack, FeedBACK)
admin.site.register(Comment, CommentAdmin)
