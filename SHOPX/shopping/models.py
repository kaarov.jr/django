from django.contrib.auth.models import User
from django.db import models
from smart_selects.db_fields import ChainedForeignKey
from django.core.exceptions import ValidationError

from client.models import Client


# Бренды или Компании
class Brand(models.Model):
    name = models.CharField('Имя компании', max_length=50)

    class Meta:
        verbose_name = 'Фирма|Бренд'
        verbose_name_plural = 'Фирмы|Бренды'
        ordering = ['id']

    def __str__(self):
        return self.name


# Город
class City(models.Model):
    name = models.CharField(max_length=80, verbose_name='Город')

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = "Городы"
        ordering = ['id']

    def __str__(self):
        return self.name


# Категории. Пример Вода
class Category(models.Model):
    name = models.CharField(max_length=100, verbose_name='Имя категории')
    city = models.ManyToManyField(City, verbose_name='Выбор Города')
    image = models.ImageField(verbose_name='ФОТО', upload_to='category_image/')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ['id']

    def __str__(self):
        return self.name


#  Под-категория. Пример Газировка
class SubCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, verbose_name='Выбор Категории')
    name = models.CharField(max_length=100, verbose_name='Имя Под-Категории')
    active = models.BooleanField(verbose_name='Активный', default=True)
    image = models.ImageField(verbose_name='ФОТО', upload_to='subcategory_image/')

    class Meta:
        verbose_name = 'Под-Категория'
        verbose_name_plural = 'Под-Категории'
        ordering = ['id']

    def __str__(self):
        return self.name


# Главный продукт. Пример Кола/Пепси ...
class Product(models.Model):
    currency = [
        ('SOM', 'Сом'),
        ('USD', 'Доллар'),
        ('EURO', 'Евро'),
        ('RUB', 'Рубль'),
    ]
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, verbose_name='Категория')
    sub_category = ChainedForeignKey(SubCategory,
                                     chained_field='category',
                                     chained_model_field='category',
                                     show_all=False,
                                     auto_choose=True,
                                     sort=True,
                                     verbose_name='Под-категория')
    brand = models.ForeignKey(Brand, on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=100, verbose_name='Имя продукта')
    description = models.TextField(verbose_name='Описание')
    image = models.ImageField(verbose_name='ФОТО', upload_to='image/')
    cur = models.CharField(max_length=4, choices=currency, default='SOM', verbose_name='Волюта')
    price = models.IntegerField(verbose_name='Цена')
    amount = models.PositiveIntegerField(verbose_name='Кол-во', default=1)
    article = models.IntegerField(verbose_name='Артикл', unique=True, null=True)
    active = models.BooleanField(verbose_name='Активный', default=True)
    date = models.DateField(verbose_name='Дата публикации', auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
        ordering = ['id']

    def __str__(self):
        return '{name} {sub_category}'.format(name=self.name, sub_category=self.sub_category)


# Заказ Покупателья
class Order(models.Model):
    name = models.ForeignKey(verbose_name='Имя Покупателья', to=Client, on_delete=models.SET_NULL, null=True)
    PAY_TYPE = {
        ('CASH', 'Наличными'),
        ('CARD', 'Карта'),
    }
    pay = models.CharField(max_length=20, verbose_name='Оплата', choices=PAY_TYPE, null=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, verbose_name='Категория')
    sub_category = ChainedForeignKey(SubCategory,
                                     chained_field='category',
                                     chained_model_field='category',
                                     show_all=False,
                                     auto_choose=True,
                                     sort=True,
                                     verbose_name='Под-категория')
    # product = ChainedForeignKey(Product,
    #                             chained_field='sub_category',
    #                             chained_model_field='sub_category',
    #                             show_all=False,
    #                             auto_choose=True,
    #                             sort=True,
    #                             verbose_name='Продукт')
    quality = models.IntegerField('Количество')

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ['id']

    def __str__(self):
        return '{name}'.format(name=self.name)


# Заказанный Продукт
class ItemOrder(models.Model):
    product = models.ForeignKey(Product, models.SET_NULL, null=True, verbose_name='Продукт')
    quantity = models.IntegerField(verbose_name='Кол-во', default=1)
    order = models.ForeignKey(Order, models.SET_NULL, null=True, verbose_name='Заказ')

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ['id']

    def __str__(self):
        return '{product} {order}'.format(product=self.product, order=self.order)


class FeedBack(models.Model):
    name = models.CharField(max_length=100, verbose_name='Имя Клиента')
    description = models.TextField(verbose_name='Описание')
    image = models.ImageField(verbose_name='ФОТО', upload_to='feedback/')
    date = models.DateField(verbose_name='Дата публикации', auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
        ordering = ['id']

    def __str__(self):
        return self.name


class Comment(models.Model):
    name = models.TextField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Коментария'
        verbose_name_plural = 'Коментарии'
        ordering = ['id']

    def __str__(self):
        return self.name

