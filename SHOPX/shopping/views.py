from datetime import datetime
import random
from time import timezone
import smtplib

from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.shortcuts import render, redirect
from rest_framework.authtoken.models import Token
from django.contrib.auth.hashers import check_password
from rest_framework.decorators import api_view, action
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .forms import *
from .serializers import *


# def test(request):
#     category = Category.objects.all().values('id', 'name')
#     result = list(category)
#     return HttpResponse(json.dumps(result), content_type='application/json')
# def test2(request):
#     subcategory = SubCategory.objects.all().values('id', 'name', 'active', 'category').order_by('-id').filter(id__gte=5,                                                                                                     category__id__gte=2)
#     result = list(subcategory)
#     return HttpResponse(json.dumps(result), content_type='application/json')
# def test3(request):
# product = Product.objects.all().values('id', 'name', 'description', 'price', 'article', 'active', 'category')
# .order_by('-id').filter(id__gte=5, category__id=2)
# product = Product.objects.all().values('id', 'name', 'description', 'price', 'article', 'active', 'category',
#                                        'sub_category').order_by('-id').filter(id__gte=5, category__id=2,
#                                                                               sub_category__id__gte=3)
# result = list(product)
# return HttpResponse(json.dumps(result), content_type='application/json')
# def bigtest(request, id):
#     product = Product.objects.all().values('id', 'name', 'description', 'price', 'article', 'active', 'category',
#                                            'sub_category').filter(sub_category_id=id)
#     result = list(product)
#     return HttpResponse(json.dumps(result), content_type='application/json')
# def info(request, name):
#     product = Product.objects.filter(name=name)
#     return render(request, 'index.html', context={'product': product})


# Test 2 -- 06.07
def home(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, 'You are logined!')
            # return redirect('login_own')
        else:
            messages.success(request, 'You are not logined!')
            # return redirect('login_own')
    product = Product.objects.all().order_by('name')
    return render(request, 'home.html', context={'product': product})


def category(request):
    category = Category.objects.all().order_by('name')
    return render(request, 'category.html', context={'category': category})


def subcategory(request):
    subcategory = SubCategory.objects.all().order_by('name')
    return render(request, 'subcategory.html', context={'subcategory': subcategory})


def product(request):
    product = Product.objects.all().order_by('date')
    return render(request, 'product.html', context={'product': product})


def private_product(request, pp):
    private_product = Product.objects.get(pk=pp)
    context = {
        'private_product': private_product
    }
    if request.method == 'POST':
        name = request.POST['name']
        comment = Comment(
            name=name,
            product=private_product,
            user=request.user
        )
        comment.save()
        return redirect('private_product', pp=pp)
    return render(request, 'private_product.html', context)


def private_product_edit(request, pp, c):
    private_product = Product.objects.get(pk=pp)
    private_product_comment = Comment.objects.get(pk=c)
    context = {
        'private_product': private_product,
        'private_product_comment': private_product_comment
    }
    if request.method == 'POST':
        name = request.POST['name']
        private_product_comment.name = name
        private_product_comment.save()
        return redirect('private_product', pp=pp)
    return render(request, 'private_product_edit.html', context)


def category_subcategory(request, cs):
    category_subcategory = SubCategory.objects.all().order_by('name').filter(category__id=cs)
    return render(request, 'category_subcategory.html', context={'category_subcategory': category_subcategory})


def category_subcategory_product(request, sc, pro):
    category_subcategory_product = Product.objects.all().order_by('name').filter(category__id=sc, sub_category__id=pro)
    return render(request, 'category_subcategory_product.html',
                  context={'category_subcategory_product': category_subcategory_product})


def subcategory_product(request, sp):
    subcategory_product = Product.objects.all().order_by('name').filter(sub_category_id=sp)
    return render(request, 'subcategory_product.html', context={'subcategory_product': subcategory_product})


# def feedback(request):
#     if request.method == 'POST':
#         name = request.POST['name']
#         text = request.POST['text']
#         img = request.POST['img']
#         test = FeedBack()
#         test.name = name
#         test.description = text
#         test.image = img
#         test.save()
#     # return redirect('feedback.html')
#     return render(request, 'add.html')


def add(request):
    category = CategoryForm()
    if request.method == 'POST':
        form = CategoryForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('add')
    return render(request, 'add.html', {"category": category})


EMAIL_ADDRESS = "anasbinkurban@gmail.com"
EMAIL_PASSWORD = "viahpurhpmniouhb"


# def add(request):
#     if request.method == 'POST':
#         cat_name = request.POST['name']
#         cat_city = request.POST['text']
#         cat_image = request.POST['img']
#         add_category = Category()
#         add_category.name = cat_name
#         add_category.city = cat_city
#         add_category.image = cat_image
#         add_category.save()
#     # return redirect('feedback.html')
#     return render(request, 'add.html', context={'add_category': add_category})


def user_comment(request, uc):
    product = Product.objects.get(id=uc)
    name = request.POST['name']
    comment = Comment(
        name=name,
        product=product,
        user=request.user,
    )
    comment.save()
    return redirect('private_product')


# first method to front
@api_view(['GET'])
def product_list_api(request):
    product = Product.objects.all()
    product_s = ProductSerializer(product, many=True)
    return Response(product_s.data, status=200)


@api_view(['GET'])
def product_get_api(request, id):
    product = Product.objects.get(id=id)
    product_s = ProductSerializer(product)
    return Response(product_s.data, status=200)


# Here started Rest Framework
class ProductListView(ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    permission_classes = [AllowAny]
    lookup_field = 'id'


# second method to front - easy method
# From this class we can see Categories and their subcategories
class CategoryViewSet(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    permission_classes = [AllowAny]

    # permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        queryset = Category.objects.all()
        serializer = CategorySerializer(queryset, many=True)
        return Response(serializer.data, status=200)

    # @action(detail=False, methods=['get'])
    # def category_all(self, request):
    #     queryset = Category.objects.all().filter(id__gt=1)
    #     return Response(CategorySerializer(queryset, many=True).data)

    def get_serializer_class(self):
        if self.action == 'list':
            self.serializer_class = CategorySerializer
        elif self.action == 'retrieve':
            self.serializer_class = CategoryDetailSerializer
        return super(CategoryViewSet, self).get_serializer_class()


# Here reverse of above. Here we can see Subcategories with Categories
class SubCategoryViewSet(ModelViewSet):
    serializer_class = SubCategory2Serializer
    queryset = SubCategory.objects.all()
    permission_classes = [AllowAny]
    lookup_field = 'id'

    # def get_serializer_class(self):
    #     if self.action == 'list':
    #         pass
    #     else:
    #         pass

    @action(detail=False, methods=['get'])
    def category_gt_2(self, request):
        queryset = Category.objects.filter(id__gt=2)
        return Response(CategoryGt2Serializer(queryset, many=True).data)


class CategoryCreateViewSet(ModelViewSet):
    serializer_class = CategoryCreateSerializer
    queryset = Category.objects.all()
    permission_classes = [AllowAny]
    lookup_field = 'id'


# Login with Token


class LoginTokenView(CreateAPIView):
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        login = request.data['login']
        password = request.data['password']
        user = User.objects.filter(username=login)
        if len(user) > 0:
            if check_password(password, user[0].password):
                token = Token.objects.filter(user=user[0])
                if token:
                    token.delete()
                token = Token(user=user[0])
                token.save()
                return Response({
                    'token': token.key,
                    'user_id': user[0].id
                })
            else:
                return Response({
                    'error': "password not true"
                })

        return Response({
            'error': "login is not True"
        })


class SendSmsToPhoneView(ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        phone = request.data['phone']
        user = User.objects.filter(last_name=phone)
        code = str(random.randint(1000, 9999))
        now = datetime.datetime.now(timezone.utc)
        if user:
            user = user[0]
            if user.last_login and user.last_login + datetime.timedelta(minutes=1) < now:
                user.last_login = now
                user.first_name = code
                user.save()
            else:
                return Response({'error': "anan jonotosun!"}, status=400)
        else:
            new_user = User(last_name=phone, first_name=code, is_active=True)
            new_user.save()
        return Response({'code': code})


#
#
class LoginWithPhoneView(ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        phone = request.data['phone']
        code = request.data['code']
        user = User.objects.filter(username=phone, first_name=code)
        if len(user) > 0 and len(user[0].first_name) > 3:
            user[0].first_name = ''
            user[0].save()
            token = Token.objects.filter(user=user[0])
            if token:
                token.delete()
            token = Token(user=user[0])
            token.save()
            return Response({
                'token': token.key,
                'user_id': user[0].id
            })
        else:
            return Response({
                'error': "Code not correct"
            })


# class SentEmailToGmail2(ModelViewSet):
#     queryset = User.objects.all()
#     permission_classes = [AllowAny]
#
#     def create(self, request, *args, **kwargs):
#         login = request.data['login']
#         password = request.data['password']
#         email = request.data['email']
#         code = str(random.randint(1000, 9999))
#
#         # def send_email(subject, msg, receiver):
#         #     try:
#         #         server = smtplib.SMTP('smtp.gmail.com:587')
#         #         server.ehlo()
#         #         server.starttls()
#         #         server.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
#         #         message = 'Subject: {}\n{}'.format(subject, msg)
#         #         server.sendmail(EMAIL_ADDRESS, receiver, message)
#         #         server.quit()
#         #         return Response({
#         #             'error': "The message was sent successfully!"
#         #         })
#         #     except:
#         #         return Response({
#         #             'error': "Check your login or password please!"
#         #         })
#
#         user = User.objects.filter(username=login)
#
#         if len(user) > 0:
#             if check_password(password, user[0].password):
#
#                 subject = "Your Password!"
#                 msg = f"{email} your password is {code}!"
#
#                 try:
#                     server = smtplib.SMTP('smtp.gmail.com:587')
#                     server.ehlo()
#                     server.starttls()
#                     server.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
#                     message = 'Subject: {}\n{}'.format(subject, msg)
#                     server.sendmail(EMAIL_ADDRESS, email, message)
#                     server.quit()
#
#                     return Response({
#                         'error': "The message was sent successfully!"
#                     })
#                 except:
#                     return Response({
#                         'error': "Check your gmail login or password please!"
#                     })
#             else:
#                 return Response({
#                     'error': "password not true"
#                 })
#
#         return Response({
#             'error': "login is not True"
#         })


class SentEmailToGmail(ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        username = request.data['name']
        email = request.data['email']
        code = str(random.randint(1000, 9999))
        now = datetime.now()

        if len(email) > 0:
            subject = "Your Password!"
            msg = f"{username} your password is {code}!"

            try:
                server = smtplib.SMTP('smtp.gmail.com:587')
                server.ehlo()
                server.starttls()
                server.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
                message = 'Subject: {}\n{}'.format(subject, msg)
                server.sendmail(EMAIL_ADDRESS, email, message)
                server.quit()

                new_user = User(date_joined=now, username=username, email=email, first_name=code)
                new_user.save()

                return Response({
                    'error': "The message was sent successfully!"
                })
            except:
                return Response({
                    'error': "Check your gmail login or password please!"
                })
        return Response({
            'error': "login is not True"
        })


class LoginCodeView(ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        name = request.data['name']
        password = request.data['password']
        email = request.data['email']
        code = request.data['code']
        user = User.objects.filter(username=name, email=email, first_name=code)
        now = datetime.now()

        if len(user) > 0 and len(user[0].first_name) == 4:
            user[0].first_name = ''
            user[0].save()
            token = Token.objects.filter(user=user[0])
            if token:
                token.delete()
            token = Token(user=user[0])
            token.save()
            if user[0].last_login:
                user[0].last_login = ''
            user[0].last_login = now
            user[0].set_password(password)
            user[0].save()
            return Response({
                'token': token.key,
                'user_id': user[0].id
            })
        else:
            return Response({
                'error': "Code or Email not correct"
            })
