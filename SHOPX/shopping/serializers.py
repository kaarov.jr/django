from rest_framework import serializers

from shopping.models import *


# first method
class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


# second method - the most easy method
class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SubCategory
        fields = ['id', 'name', 'image']


class CategorySerializer(serializers.ModelSerializer):
    sub_categories = serializers.SerializerMethodField('get_sub_categories')

    class Meta:
        model = Category
        fields = ['name', 'city', 'image', 'sub_categories']

    def get_sub_categories(self, obj):
        sub_categories = SubCategory.objects.filter(category=obj)
        return SubCategorySerializer(sub_categories, many=True).data


#   ----------


class CategoryDetailSerializer(serializers.ModelSerializer):
    sub_categories = serializers.SerializerMethodField('ssub_categories')

    class Meta:
        model = Category
        fields = ['name', 'sub_categories']

    def ssub_categories(self, obj):
        sub_categories = SubCategory.objects.filter(category=obj)
        return SubCategorySerializer(sub_categories, many=True).data


class Category2Serializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['city', 'name', 'image']


class SubCategory2Serializer(serializers.ModelSerializer):
    category = Category2Serializer()

    class Meta:
        model = SubCategory
        fields = ['id', 'name', 'image', 'category']


class CategoryGt2Serializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'city', 'name', 'image']


class CategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['name']

    def validate(self, attrs):
        if len(attrs['name']) < 5:
            raise serializers.ValidationError("Category name must be long 5!")
        return attrs
