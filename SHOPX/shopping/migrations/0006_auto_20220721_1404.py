# Generated by Django 3.2 on 2022-07-21 08:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0005_alter_order_pay'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='category_image/', verbose_name='ФОТО'),
        ),
        migrations.AlterField(
            model_name='order',
            name='pay',
            field=models.CharField(choices=[('CASH', 'Наличными'), ('CARD', 'Карта')], max_length=20, null=True, verbose_name='Оплата'),
        ),
    ]
