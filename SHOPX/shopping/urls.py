from django.urls import path, include
from shopping.views import *
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'category', CategoryViewSet)
router.register(r'subcategory', SubCategoryViewSet)
router.register(r'createcategory', CategoryCreateViewSet)

urlpatterns = [
    # path('bigtest/<int:id>/', bigtest),
    # path('pro/<str:name>/product', info, name='info'),
    # path('test/', test),
    # path('test2/', test2),
    # path('test3/', test3),

    # path('', home, name='home'),
    # path('categoty/', category, name='category'),
    # path('subcategoty/', subcategory, name='subcategory'),
    # path('product/', product, name='product'),
    # path('category_subcategory/<int:cs>/', category_subcategory, name='category_subcategory'),
    # path('category_subcategory_product/<int:sc>/<int:pro>/', category_subcategory_product,
    #      name='category_subcategory_product'),
    # path('subcategory_product/<int:sp>/', subcategory_product, name='subcategory_product'),

    # path('private_product/<int:pp>/', private_product, name='private_product'),
    # path('private_product_edit/<int:pp>/<int:c>/', private_product_edit, name='private_product_edit'),
    # path('add/', add, name='add'),
    # path('user_comment/<int:uc>/', user_comment, name='user_comment'),
    #   ----------
    # path('product_list', product_list_api),
    # path('product_get/<int:id>', product_get_api),
    #   ----------
    path('product_list_class/', ProductListView.as_view({'get': 'list', 'post': 'create'})),
    path('product_list_class/<int:id>/', ProductListView.as_view({'get': 'retrieve', 'put': 'update'})),
    #   ----------
    path('', include(router.urls)),
    #   ----------
    path('token/', LoginTokenView.as_view()),
    path('send/sms/', SendSmsToPhoneView.as_view({'post': 'create'})),
    path('check/sms/', LoginWithPhoneView.as_view({'post': 'create'})),
    path('send/email/', SentEmailToGmail.as_view({'post': 'create'})),
    path('check/code/', LoginCodeView.as_view({'post': 'create'})),
]
