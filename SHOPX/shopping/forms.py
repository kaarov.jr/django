from .models import *
from django.forms import ModelForm


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = ['name', 'image', 'city']
