from django import template
from shopping.models import *

register = template.Library()


@register.simple_tag()
def get_categories(filter=None):
    if not filter:
        return Category.objects.all()
    else:
        return Category.objects.all().filter(pk=filter)


@register.simple_tag()
def get_subcategories():
    return SubCategory.objects.all()


@register.simple_tag()
def get_products():
    return Product.objects.all()


@register.inclusion_tag('tags/list_categories.html')
def show_categories():
    category = Category.objects.all()
    return {'category': category}


@register.inclusion_tag('tags/list_products.html')
def show_products():
    product = Product.objects.all()
    return {'product': product}
